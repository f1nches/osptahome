<?php

$modules = get_field('section_modules');
$docRoot = get_template_directory() . '/';

if (!empty($modules)) {
    // include('modules/_global_module_functions.php');
    foreach ($modules as $module) {
        $moduleName = $module['acf_fc_layout'];
        $moduleFile = 'partials/modules/_' . $moduleName . '.php';
        $moduleFileCheck = $docRoot . $moduleFile;
        
        if (file_exists($moduleFileCheck)) {
            include($moduleFile);
        } else {
            trigger_error('The page module, ' . $moduleFileCheck . ' (' . ucwords(str_replace('_', ' ', $moduleName)) . ') , is currently not set up in the acf_modules.php file. This could also imply that the module was not built.');
        } // end if
    } // end foreach
} // end if
?>