<?php

function ospta_get_locations_in_radius() {
    $radius = $_POST['radius'];
    $zip = $_POST['zip'];

    global $wpdb;
    $query = "SELECT * FROM wp_posts WHERE post_type = 'locations' AND post_status = 'publish' ORDER BY post_title";
    $res = $wpdb->get_results($query);
    $out = [];
    $dests = '';

    $coord_to_id_map = [];
    $filtered_ids = [];

    foreach ($res as $i => $location) {
        $coord = get_field('latitude', $location->ID) . ',' . get_field('longitude', $location->ID);
        $coord_to_id_map[$i] = $location->ID;

        $out[] = $coord;
    }

    $dests = implode("|", $out);

    $radius_converted = 1760 * $radius;


    $key = get_field('google_maps_distance_matrix_api_key', 'options');

    $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' . $zip . '&destinations=' . $dests . '&key=' . $key;

    $data = file_get_contents($url);
    $resp = json_decode($data);

    foreach ($resp->rows as $row) {
        foreach ($row->elements as $i => $element) {
            $info = $element;

            $dist = $info->distance;

            if ($dist->value <= $radius_converted) {
                $filtered_ids[] = $coord_to_id_map[$i];
            }
        }
    }


    echo json_encode($filtered_ids);
    wp_die();
}

add_action('wp_ajax_nopriv_get_locations_in_radius', 'ospta_get_locations_in_radius');
add_action('wp_ajax_get_locations_in_radius', 'ospta_get_locations_in_radius');

function ospta_zip_to_latlng() {
    $zip = $_POST['zip'];
    $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address=' . $zip . '&sensor=false');
    $output = json_decode($geocode);
    $latitude = $output->results[0]->geometry->location->lat;
    $longitude = $output->results[0]->geometry->location->lng;

    echo json_encode(['lat' => $latitude, 'lng' => $longitude]);
    wp_die();
}

add_action('wp_ajax_nopriv_zip_to_latlng', 'ospta_zip_to_latlng');
add_action('wp_ajax_zip_to_latlng', 'ospta_zip_to_latlng');

function ospta_get_locations() {
    global $wpdb;
    $query = "SELECT * FROM wp_posts WHERE post_type = 'locations' AND post_status = 'publish' ORDER BY post_title";
    $res = $wpdb->get_results($query);
    $out = [];
    foreach ($res as $i => $location) {
        $tmp = (object) [];
        $tmp->name = $location->post_title;
        $tmp->url = site_url() . '/locations/' . $location->post_name;
        $tmp->id = $location->ID;

        $fields = get_fields($location->ID);
        foreach ($fields as $field => $value) {
            $tmp->{$field} = $value;
        }

        $tmp->services = ospta_convert_services_to_array($tmp->location_services);
        $tmp->therapists = ospta_convert_therapists_to_array($tmp->location_physical_therapists);
        $tmp->county = get_field('county', $tmp->id);

        $out[] = $tmp;
    }


    echo json_encode($out);
    wp_die();
}

function ospta_get_services() {
    global $wpdb;
    $query = "SELECT * FROM wp_posts WHERE post_type = 'locations' AND post_status = 'publish' ORDER BY post_title";
    $res = $wpdb->get_results($query);
    $out = [];
    foreach ($res as $i => $location) {
        $tmp = (object) [];
        $fields = get_fields($location->ID);
        foreach ($fields as $field => $value) {
            $tmp->{$field} = $value;
        }

        if (!is_array($tmp->location_services))
            continue;

        foreach ($tmp->location_services as $service) {
            $s = $service['service'];
            if (empty($s))
                continue;

            $out[sanitize_title($s)] = $s;
        }
    }

    ksort($out);

    return $out;
}

function ospta_convert_services_to_array($services) {
    $out = [];


    foreach ($services as $service) {
        $s = $service["service"];

        if (empty($s))
            continue;

        $slug = sanitize_title($s);

        $out[$slug] = $s;
    }
    return $out;
}

function ospta_convert_therapists_to_array($therapists) {
    $out = [];

    foreach ($therapists as $therapist) {
        $t = $therapist["physical_therapist"];
        $out[$t->post_name] = $t->post_title;
    }

    return $out;
}

add_action('wp_ajax_nopriv_get_locations', 'ospta_get_locations');
add_action('wp_ajax_get_locations', 'ospta_get_locations');

function ospta_set_lat_lng($post_id) {
    $post = get_post($post_id);

    if ($post->post_type == 'locations') {
        $add = get_field('location_address');
        $city = get_field('location_city');
        $state = get_field('location_state');
        $zip_code = get_field('location_zip_code');

        $address = $add . ', ' . $city . ' ' . $state . ', ' . $zip_code;

        $prepAddr = str_replace(' ', '+', $address);
        $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address=' . $prepAddr . '&sensor=false');
        $output = json_decode($geocode);
        $latitude = $output->results[0]->geometry->location->lat;
        $longitude = $output->results[0]->geometry->location->lng;

        update_field('latitude', $latitude, $post_id);
        update_field('longitude', $longitude, $post_id);
    }
}

add_action('acf/save_post', 'ospta_set_lat_lng', 20);

function ospta_the_top_nav_active_class($link) {
    global $post;

    $slug = $post->post_name;

    $slash = explode("/", $slug);

    if (count($slash) > 1) {
        $slug = $slash[0];
    }

    if ($link == $slug) {
        echo ' class="active"';
    }
}

function ospta_services_list($splitterClass = 'col-md-6', $columns = 2, $currentService = false) {
    echo '<ul class="services-list no-style-list">';
    $services = get_posts(array(
        'post_type' => 'service',
        'post_status' => 'publish',
        'posts_per_page' => -1
    ));

    $totalServices = count($services);
    $servicesCount = 1;
    $itemsPerColumn = ceil($totalServices / $columns);

    foreach ($services as $service) {
        $servicesLink = $service->guid;
        $serviceSlug = $service->post_name;
        $serviceName = $service->post_title;

        if (notEmpty($splitterClass) && intval($servicesCount % $itemsPerColumn) === 1) {

            if (intval($servicesCount % $itemsPerColumn) === 1 && $servicesCount !== 1) {
                echo '</div>';
            }

            echo '<div class= "' . $splitterClass . '">';
        }

        if (notEmpty($currentService) && $currentService === $serviceSlug) {
            $currentListClass = ' current-service';
        } else {
            $currentListClass = '';
        }

        echo '<li class="list-item' . $currentListClass . '"><a href="' . $servicesLink . '">' . $serviceName . '</a></li>';

        if (notEmpty($splitterClass) && $servicesCount === $totalServices) {
            echo '</div>';
        }

        $servicesCount++;
    }
    echo '</ul>';
}

function employee_files() {
    if (have_rows('file_section')) {

        while (have_rows('file_section')) {
            the_row();
            ?>
            <div class="row" style="margin-top: 20px;">
                <h3><?= get_sub_field('title') ?></h3>
                <div class="col-container">
                <?php
                while (have_rows('files')) {
                    the_row();
                    $name = get_sub_field('name'); 
                    $file = get_sub_field('file');
                    ?>
                  <div class="row-sm-2 col-sm-6 file-entry-wrapper">
                    <a href="<?=$file?>">
                        <div class="file-entry">
                            <span class="file-title"><i class="fa fa-file" style="margin-right: 10px;"></i> <?=!empty($name) ? $name : $file ?></span>
                        </div>
                        
                    </a>
                  </div>
                <?php } ?>
              </div>    
            </div>
            <?php
        }
    }
}

add_shortcode('employee_files', 'employee_files');