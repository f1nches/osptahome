<?php
/************
 * ENQUEUES *
 ************/

function basetheme_enqueues() {
  wp_enqueue_style('slick_styles', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css');
  wp_enqueue_style('main_styles', get_template_directory_uri().'/dist/styles/main.min.css');
  wp_enqueue_script('slick_scripts', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', ['jquery'], '', true);
  wp_enqueue_script('main_scripts', get_template_directory_uri().'/dist/scripts/main.min.js', ['jquery'], '', true);
} // end function basetheme_enqueues
add_action('wp_enqueue_scripts', 'basetheme_enqueues');

/******************
 * REGISTER MENUS *
 ******************/

register_nav_menus(
  array (
    'main_nav' => 'Main Navigation Bar/Dropdown'
  )
);

function varInfo($var, $method = 'print') {
  echo '<pre>';
  
  if ($method === 'print') {
    print_r($var);
  } elseif ($method === 'dump') {
    var_dump($var);
  } // end if
  
  echo '</pre>';
} // end function varInfo

/***********************
 * ACF - OPTIONS PAGES *
 ***********************/

if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => 'Theme General Settings',
    'menu_title'  => 'Theme Settings',
    'menu_slug'   => 'theme-general-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Theme Header Settings',
    'menu_title'  => 'Header',
    'parent_slug' => 'theme-general-settings',
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Theme Footer Settings',
    'menu_title'  => 'Footer',
    'parent_slug' => 'theme-general-settings',
  ));
  
}

add_filter('show_admin_bar', '__return_false');