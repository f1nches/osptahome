<?php
/*******************
 * DEBUG FUNCTIONS *
 *******************/

// Purposed for ACF fields, checks to see if field is empty or not filled in, returns bool.
function notEmpty($var) {

  if (isset($var)) {
    // Shouldn't generally recieve arrays but if it does...
    if (!is_array($var)) {
      // To make sure backend of WP doesn't have anything but spaces
      $spaceCheckedField = str_replace(' ', '', $var);
    } else {
      $spaceCheckedField = $var;
    } // end if

    if (empty($var)) {
      return false;
    } else {
      return true;
    } // end if
  } else {
    return false;
  } // end if
} // end function isEmpty

/*******************
 * MEDIA FUNCTIONS *
 *******************/

// Pulls in an array and returns an image tag with alt attribute, optional to add class(es) string
function getImage($array, $class = '', $cover = false, $coverWrapperClass = '' ) {
  $imageUrl = '<img src="'.$array['url'].'"';
  $imageAlt = ' alt="'.$array['alt'].'"';

  if (notEmpty($coverWrapperClass)) {
    $coverWrapperClass = ' '.$coverWrapperClass;
  }

  if ($cover) {

    $coverOuterOpen = '<div class="media-cover-wrapper'.$coverWrapperClass.'" style="background:url('.$array['url'].') center center / cover no-repeat">';
    $coverOuterClose = '</div>';

    if (notEmpty($class)) {
      $coverClass = ' media-cover hide';
    } else {
      $coverClass = 'class="media-cover hide"';
    } // end if

  } else {
    $coverOuterOpen = '';
    $coverOuterClose = '';
    $coverClass = '';
  } // end if

  if (notEmpty($class)) {
    $imageClass = ' class="'.$class.$coverClass.'">';
  } else {
    $imageClass = $coverClass.'>';
  } // end if

  $imageTag = $coverOuterOpen.$imageUrl.$imageAlt.$imageClass.$coverOuterClose;

  return $imageTag;
} // end function getImage

function theImage($array, $class = '', $cover = false, $coverWrapperClass = '') {
  echo getImage($array, $class, $cover, $coverWrapperClass);
} // function theImage