<?php

/**********************************************
 * MOST RELATED POSTS - by most related terms *
 **********************************************/

function get_most_related_posts($customOptions = array()) {
global $post;
global $wpdb;
$orig_post = $post;

$defaultOptions = array(
  'related_to' => 'post_id',
  'post_id' => $post->ID,
  'term_ids' => array(),
  'post_type' => 'post',
  'post_status' => 'publish',
  'posts_to_return' => -1,
  'order_by' => '',
  'order' => 'ASC'
);

$options = array_merge($defaultOptions, $customOptions);
$related_to = $options['related_to'];
$post_type = $options['post_type'];
$post_status = $options['post_status'];
$posts_to_return = $options['posts_to_return'];
$order_by_type = $options['order_by'];


if ($related_to === 'post_id') {
  $post_id = $options['post_id'];
  $terms = wp_get_post_tags($post_id);
  $term_ids = array();

  foreach($terms as $individual_term) {
    $term_ids[] = $individual_term->term_id;
  }
} else if ($related_to === 'term_id') {
  $term_ids = $options['term_ids'];
}

if (!empty($order_by_type)) {
  $order = $options['order'];
  $order_by = ', '.$wpdb->posts.'.'.$order_by_type.' ';
  $order_by .= $order;
} else {
  $order_by = '';
}

if (!empty($term_ids)) {
  $query = "
    SELECT ".$wpdb->posts.".*, COUNT(".$wpdb->posts.".ID) as q
    FROM ".$wpdb->posts." INNER JOIN ".$wpdb->term_relationships."
    ON (".$wpdb->posts.".ID = ".$wpdb->term_relationships.".object_id)
    WHERE ".$wpdb->posts.".ID NOT IN (".$post->ID.")
    AND ".$wpdb->term_relationships.".term_taxonomy_id IN (".implode(",",$term_ids).")
    AND ".$wpdb->posts.".post_type = '".$post_type."'
    AND ".$wpdb->posts.".post_status = '".$post_status."'
    GROUP BY ".$wpdb->posts.".ID
    ORDER BY q DESC".$order_by;
    if ($posts_to_return !== -1) {
    $query .= " LIMIT ".$posts_to_returns;
    }
  $related_posts = $wpdb->get_results($query, OBJECT);
}

$post = $orig_post;
return $related_posts;
}