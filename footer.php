<?php
  global $options;
?>

    <div class="footer-push">
    </div>
    </div>
    <footer class="footer">
      <div class="footer-main">
        <div class="container">
          <div class="col-lg-3 logo-area">
              <a href="<?= site_url() ?>"><img src="<?= get_template_directory_uri() ?>/assets/images/logo_footer.svg" class="logo" alt="OSPTA"></a>
          </div>
          <div class="footer-nav-wrapper col-sm-6 col-lg-6 footer-col" style="border-right: 0;">
            <ul class="footer-nav no-style-list">
              <div class="col-lg-6">
                <li>
                    <a href="<?= site_url() ?>/home-health/">Home Health</a>
                </li>
                <li>
                    <a href="<?= site_url() ?>/hospice/">Hospice</a>
                </li>
                <li>
                    <a href="<?= site_url() ?>/insurances/">Insurances</a>
                </li>
              </div>
              <div class="col-lg-6">
                <li>
                    <a href="<?= site_url() ?>/areas-we-serve/">Areas We Serve</a>
                </li>
                <li>
                    <a href="<?= site_url() ?>/join-our-team/">Join Our Team</a>
                </li>
                <li>
                    <a href="<?= site_url() ?>/contact-us/">Contact Us</a>
                </li>
              </div>
            </ul>
          </div>
          <div class="col-sm-6 col-lg-3 services footer-col">
            <div class="meta-info show-block-sm" style="margin-top: 0;">
                <p>info@osptainc.com</p>
                <p>&copy; <?= date('Y'); ?> OPSTA Inc. All rights are reserved.</p>
            </div>
          </div>
        </div>
      </div>
      <?php if (notEmpty($options['terms_of_use'])) : ?>
        <div class="legal-area background-darker-blue color-white">
          <div class="container">
            <div class="terms-link-wrapper">
              <a class="terms-link color-white" href="<?php echo $options['terms_of_use']; ?>" target="_blank">Terms of Use</a>
              |
              <a class="terms-link color-white" href="<?php echo $options['cancellation_policy_pdf']; ?>" target="_blank">Cancellation Policy</a>
            </div>
          </div>
        </div>
      <?php endif; ?>
    </footer>
    <?php wp_footer(); ?>
  </body>
</html>
