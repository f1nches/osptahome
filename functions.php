<?php
/***************************************************************
 * WP SETUP: Enqueues, register menus, custom post types, etc. *
 ***************************************************************/
require_once('functions/setup.php');

/*********************
 * UTILITY FUNCTIONS *
 *********************/
require_once('functions/utils.php');

/**************************
 * CUSTOM QUERY FUNCTIONS *
 **************************/
include('functions/query.php');

/**************************
 * OSPTA FUNCTIONS *
 **************************/

include('functions/ospta.php');

add_filter('show_admin_bar', '__return_false');

function force_load_module($name, $module = []) {
    require get_template_directory() . '/partials/modules/_'.$name.'.php';
}
function change_wp_search_size($query) {
    if ( $query->is_search ) // Make sure it is a search page
        $query->query_vars['posts_per_page'] = 1000;

    return $query; // Return our modified query variables
}
add_filter('pre_get_posts', 'change_wp_search_size'); // Hook our custom function onto the request filter