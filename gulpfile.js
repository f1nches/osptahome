// Gulp and it's plugin required
var clean = require('del');
var gulp = require('gulp');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var glob = require('gulp-sass-glob');
var minifyCss = require('gulp-clean-css');
var jshint = require('gulp-jshint');
var jsConcat = require('gulp-concat');
var jsMinify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');

// Path object for gulp tasks

var path = {
  assets: {

    all: 'assets/**/*',

    styles: {
      all: 'assets/styles/**/*.scss',
      main: [
        'assets/styles/main.scss'
      ]
    },

    scripts: {
      all: 'assets/scripts/**/*.js',
      main: [
        './assets/scripts/begin.js',
        './assets/scripts/utils.js',
        './assets/scripts/plugins/**/*.js',
        './assets/scripts/partials/**/*.js', 
        './assets/scripts/main.js',
        './assets/scripts/end.js'
      ]
    },

    images: 'assets/images/*'

  },

  dist: {

    styles: {
      folder: './dist/styles/',
      file: 'dist/styles/main.css'
    },

    scripts: {
      folder: './dist/scripts/',
      file: 'dist/scripts/main.js'
    },

    images: 'dist/images/'

  },

  uploads: '../../uploads/**/*',
}

// Style Tasks

gulp.task('clean-styles', function(){
  return clean(path.dist.styles.folder);
});

gulp.task('clean-scripts', function(){
  return clean(path.dist.scripts.folder);
});

gulp.task('clean-images', function(){
  return clean(path.dist.images);
});

gulp.task('sass', ['clean-styles'], function() {
  return gulp.src(path.assets.styles.main)
    .pipe(glob())
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(sass({includePaths: ['./**']}))
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.dist.styles.folder))
});

gulp.task('styles', ['sass'], function(){
  return gulp.src(path.dist.styles.file, {base: './'})
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(plumber())
    .pipe(minifyCss())
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('.'));
});

//Script Tasks

gulp.task('concat', ['clean-scripts'], function(){
  return gulp.src(path.assets.scripts.main)
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(jsConcat('main.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.dist.scripts.folder));
});

gulp.task('jshint', ['concat'], function(){
  gulp.src(path.dist.scripts.file)
    .pipe(plumber())
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

gulp.task('scripts', ['jshint'], function(){
  return gulp.src(path.dist.scripts.file, {base: './'})
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(plumber())
    .pipe(jsMinify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('.'));
});

// Images folder Image Compression Task
gulp.task('images', ['clean-images'], function(){
  return gulp.src(path.assets.images)
    .pipe(imagemin({ progressive: true }))
    .pipe(gulp.dest(path.dist.images));
});

// Image Compression Task
gulp.task('uploads', function(){
  return gulp.src(path.uploads, { base: './' })
    .pipe(imagemin({ progressive: true }))
    .pipe(gulp.dest('.'));
});

// The Watch tasks

gulp.task('watch', ['styles', 'scripts'], function() {
  gulp.watch(path.assets.styles.all, ['styles']);
  gulp.watch(path.assets.scripts.all, ['scripts']);
});

// The Default Task
gulp.task('default', ['styles', 'scripts']);