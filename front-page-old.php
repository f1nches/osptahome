<?php
  $fields = get_fields();
  $options = get_fields('option');
  include('partials/head.php'); 
  get_header();
?>
<main class="interior container-md page-<?= $post->post_name ?>">
    <div class="main-callout module container-sm">
        <div class="col-sm-6 col-lg-5 intro-text-container">
          <section class="intro-text cms-wysiwyg">
            <?= $fields['intro_text']; ?>
          </section>
        </div>
        <div class="col-sm-6 col-lg-7 services-list-container">
            <section class="services-list-wrapper">
              <h2>Services</h2>
              <?php ospta_services_list('col-lg-6'); ?>
            </section>
        </div>
    </div>
    <section class="site-areas container-md small-max">

        <div class="col-sm-4 area locations-area">
            <a href="<?=site_url()?>/locations/">
              <div class="image-container">
                <img src="<?=get_template_directory_uri()?>/dist/images/location_icon.svg" alt="Location Finder Icon">
              </div>
              <span class="area-title color-darker-blue">Location Finder</span>
            </a>
        </div>
        <div class="col-sm-4 area patient-center-area">
            <a href="<?=site_url()?>/patient-center/">
              <div class="image-container">
                <img src="<?=get_template_directory_uri()?>/dist/images/patient_center_icon.svg" alt="Patient Center Icon">
              </div>
              <span class="area-title color-darker-blue">Patient Center</span>
            </a>
        </div>
        <div class="col-sm-4 area pay-bill-area">
            <a href="https://heartlandpaymentservices.net/PaymentPortal/OrthopedicAndSportsPhysical/Bills">
              <div class="image-container">
                <img src="<?=get_template_directory_uri()?>/dist/images/pay_bill_icon.svg" alt="Pay Bill Icon">
              </div>
              <span class="area-title color-darker-blue">Pay Bill</span>
            </a>
        </div>
    </section>
    <section class="last-callouts container-sm">
      <div class="col-sm-6">
        <div class="last-callout-wrapper faqs">
          <a href="<?=site_url()?>/faqs/">FAQs</a>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="last-callout-wrapper employment">
          <a href="<?=site_url()?>/join-our-team/">EMPLOYMENT</a>
        </div>
      </div>
    </section>
</main>
<?php get_footer(); ?>