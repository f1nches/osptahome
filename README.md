# Base Theme Docs
## Table of Contents
- [Definitions](#markdown-header-definitions)
    - [Project Naming Standard](#markdown-header-project-naming-standard)
- [Initial Set Up](#markdown-header-initial-set-up)
    - [Manual New Site Install](#markdown-header-manual-new-site-install)
    - [Manual Existing Site Install](#markdown-header-manual-existing-site-install)

## Definitions 
[Table of Contents](#markdown-header-table-of-contents)

### Project Naming Standard 
[Table of Contents](#markdown-header-table-of-contents)

- The url of the project's live site, without the top-level domain (*e.g.* leave out .com).

## Initial Set Up 
[Table of Contents](#markdown-header-table-of-contents)

### Manual Existing Site Install 
[Table of Contents](#markdown-header-table-of-contents)

### Manual New Site Install 
[Table of Contents](#markdown-header-table-of-contents)

1. In the top toolbar, from within bitbucket.org, go to Projects->Clients. Make sure it is under the Panoptic Creations Team.
2. On the left toolbar, from within bitbucket.org, go to Actions (also maybe presented as 3 dots)->Create Repository.
3. Make sure the following settings are filled in under Create a new repository:
  - Owner: panopticcreations
  - Project: Clients
  - Repository Name: the [Project Naming Standard](#markdown-header-project-naming-standard)
  - Access Level: Check "This is a private repository"
  - Repository Type: Select "Git"
4. Click Create Repository
5. Keep the window you are directed to open, you will need to return to it later. In a new tab, go to the [WordPress Site](https://www.wordpress.org).
5. Download, and unzip, the latest version of wordpress into your sites folder, found in your home directory.
6. Rename the wordpress folder to the [Project Naming Standard](#markdown-header-project-naming-standard).
7. Delete all themes provided in the themes folder by default.
8. From within your command line, change directory to the themes folder.
9. In bitbucket go to the basetheme repository and copy the basetheme repo URL that is found in the upper right hand corner to the right the label "HTTPS".
10. Write in the following command:
  `git clone [the basetheme repo URL that you copied]`
11. Once the repo has downloaded cd to the now created basetheme folder:
  `cd basetheme`
12. We will be deleting the git connection to basetheme with the following command:
  **Unix/Terminal** `sudo rm .git`
  **Command Prompt** `DEL /F .git`
13. From here you will install the node modules used for gulp, and make your initial gulp. So, in the command line issue the following command:
  `npm install && gulp`
13. In your text editor, open the root directory of the project, which is the new folder you created in the sites directory that you renamed to the [Project Naming Standard](#markdown-header-project-naming-standard).
14. Create a .gitignore file with the following contents:
  `.DS_Store
    node_modules/
    node_modules/*
    search-replace/
    search-replace/*`
15. In the command line, change directory back to this root folder:
  **Unix/Terminal** `cd ~/sites/[newprojectname]`
  **Command Prompt** `cd %HOMEPATH%\sites\[newprojectname]`
16. Now, you will begin set up for a connection of these new site files with the new repository you created in bitbucket earlier. Go back to that repository in bitbucket now, and just like you did with the base theme, copy this new repository's repo url, found next to the "HTTPS" label.
17. You will initialize the local git with the following command in the command line:
  `git init && git add . && git commit -m "Initial commit for repository"`
18. You will now connect this local git repository with the remote one:
  `git remote add origin [ the new repo's repo url you just copied] && git push -u origin master`
19. Finally set up your local database to view this site locally (instruction coming soon...)