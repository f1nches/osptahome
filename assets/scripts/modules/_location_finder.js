(function ($) {
    ospta.locationFinder = {
        _locations: {},
        _map: null,
        _baseURL: '',
        init: function (lat, lng, base_url) {
            this._map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: lat, lng: lng},
                scrollwheel: false,
                zoom: 12
            });

            this._baseURL = base_url;

            this._getLocationsAJAX();
            this._bindEvents();
        },
        _bindEvents: function () {
            var self = this;
            $('#search').on('click', function (e) {
                e.preventDefault();
                var zip = $('#zip').val();
                var radius = $('#radius').val();
                var service = $('#service').val();
                var physician = $('#physician').val();

                self.doSearch(zip, radius, service, physician);
            });
        },
        _addLocation: function (location) {

            var marker = new google.maps.Marker({
                position: {lat: parseFloat(location.latitude), lng: parseFloat(location.longitude)},
                map: this._map,
                icon: this._baseURL + '/assets/images/marker.png'
            });

            var infowindow = new google.maps.InfoWindow({
                content: "<h3>"+ location.name +"</h3><br>"+location.location_address+"<br>"+location.location_city+", "+location.location_state+" " + location.location_zip_code+"<br><a href='"+ location.url +"'>View More Information</a>"
            });

            marker.addListener('click', function () {
                infowindow.open(this._map, marker);
            });

            location.marker = marker;
            location.hidden = false;

            this._locations[location.id] = location;
        },
        _hideLocations: function () {
            for (var i in this._locations) {
                var location = this._locations[i];
                if (location.hidden) {
                    location.marker.setMap(null);
                } else {
                    location.marker.setMap(this._map);
                }
            }

            return true;
        },
        _filterByService: function (service) {
            if (!service)
                return false;

            for (var i in this._locations) {
                var location = this._locations[i];
                if (location.hidden)
                    continue; // already been filtered, so move on

                location.hidden = !location.services.hasOwnProperty(service);
            }

            return true;
        },
        _filterByID: function (ids) {
            if (!ids)
                return false;

            for (var i in this._locations) {
                var location = this._locations[i];
                if (location.hidden)
                    continue; // already been filtered, so move on

                location.hidden = ids.indexOf(location.id) === -1;
            }

            return true;
        },
        _showLoader: function () {
            $('#loading-screen').fadeIn(100);
        },
        _hideLoader: function () {
            $('#loading-screen').fadeOut(100);
        },
        _filterByTherapist: function (therapist) {
            if (!therapist)
                return false;

            for (var i in this._locations) {
                var location = this._locations[i];
                if (location.hidden)
                    continue; // already been filtered, so move on

                location.hidden = !location.therapists.hasOwnProperty(therapist);
            }

            return true;
        },
        _resetHiddenStatusForLocations: function () {
            for (var i in this._locations) {
                var location = this._locations[i];

                location.hidden = false;
            }

            return true;
        },
        doSearch: function (zip, radius, service, therapist) {
            this._resetHiddenStatusForLocations();
            var self = this;

            if (zip && radius) {
                // ajax call
                this._showLoader();

                $.ajax({
                    url: ajax_url,
                    data: {action: 'get_locations_in_radius', zip: zip, radius: radius},
                    type: 'POST',
                    dataType: 'JSON'
                })
                        .success(function (ids) {
                            self._afterSearchDoCenter(zip, service, therapist, ids);
                        })
                        .always(function () {
                            self._hideLoader();
                        });

            } else {
                this._filterByService(service);
                this._filterByTherapist(therapist);
                this._hideLocations();
            }


        },
        _afterSearchDoCenter: function (zip, service, therapist, ids) {
            var self = this;

            $.ajax({
                url: ajax_url,
                data: {action: 'zip_to_latlng', zip: zip},
                type: 'POST',
                dataType: 'JSON'
            })
                    .success(function (coords) {
                        self._map.panTo(coords);
                        self._filterByID(ids);
                        self._filterByService(service);
                        self._filterByTherapist(therapist);
                        self._hideLocations();
                    })
                    .always(function () {
                        self._hideLoader();
                    });
        },
        _getLocationsAJAX: function () {
            var self = this;
            $.ajax({
                url: ajax_url,
                data: {action: 'get_locations'},
                type: 'POST',
                dataType: 'JSON'
            })
                    .success(function (locations) {
                        for (var i in locations) {
                            self._addLocation(locations[i]);
                        }
                    });
        }
    };

}(jQuery));

