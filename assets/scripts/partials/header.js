var slidesContainer = $('.slides-container');
$('.slides-container').slick({
  dots: true,
  arrows: false,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 3000,
  customPaging: function() {
    return '<i class="fa fa-circle-o"></i><i class="fa active-dot fa-circle"></i>';
  },
});