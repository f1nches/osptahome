var body = $('body');
var nav = $('nav#page-nav');
var menu = nav.find('ul#menu-primary-navigation');
var menuBars = $('#page-header .menu-bars-wrapper .fa-bars');
var parentItem = nav.find('li.menu-item-has-children');
var menuClose = nav.find('.menu-close-wrapper .fa-times');
var mobileActivated = false;
var openMobileNav = false;

function closeMobileMenu() {
  nav.removeClass('open-mobile-nav');
  body.css('overflow', '');
  nav.css('overflow-y', '');
  openMobileNav = false;
  var openedItems = menu.find('.opened');
  openedItems.find('ul.sub-menu').slideUp();
  openedItems.removeClass('opened');
}

if (window.innerWidth < breakpoints.md) {
  nav.addClass('mobile-activated');
  mobileActivated = true;
  openMobileNav = true;
}

menuBars.click(function() {
  nav.addClass('open-mobile-nav');
  body.css('overflow', 'hidden');
  nav.css('overflow-y', 'auto');
  openMobileNav = true;
});

menuClose.click(function() {
  closeMobileMenu();
});

parentItem.click(function(e){
  e.preventDefault();
  var target = $(e.target);
  var targetItem = $(this);

  if (targetItem.hasClass('opened') && target.is('a')) {
    window.location = target.attr('href');
  } else {
    var otherOpenedItems = targetItem.siblings('li.opened');
    otherOpenedItems.find('ul.sub-menu').slideUp();
    otherOpenedItems.removeClass('opened');
    targetItem.addClass('opened');
    targetItem.children('ul.sub-menu').slideDown();
  }
});

$(window).resize(function(){

  if (mobileActivated && window.innerWidth >= breakpoints.md) {
    nav.removeClass('mobile-activated');
    mobileActivated = false;

    if (openMobileNav) {
      closeMobileMenu();
    }
  } else if (!mobileActivated && window.innerWidth < breakpoints.md) {
    nav.addClass('mobile-activated');
    mobileActivated = true;
  }
});

function menuToggle(funcThis, type, e) {
  var target = $(e.target);
  var menuItem = funcThis;

  if (!menuItem.hasClass('opened') && (type === 'click' || type === 'hover-in')) {
    var otherOpenItems = menuItem.siblings('.opened');
    otherOpenItems.find('.sub-menu').stop(true, true).slideUp(function(){
      otherOpenItems.find('.sub-menu').attr('style', '');
    });
    otherOpenItems.removeClass('opened');
    otherOpenItems.find('.opened').removeClass('opened');
    menuItem.addClass('opened');
    menuItem.children('.sub-menu').stop(true, true).slideDown();
    
  } else if (type === 'click' || type === 'hover-out') {
    if (type === 'hover-out') {
      menuItem.children('.sub-menu').slideUp();
      menuItem.removeClass('opened');
    } else if (target.is('a') && type === 'click') {
      var itemHref = target.attr('href');
      window.location.replace(itemHref);
    }
  }
}

var showMenuTimer;
$('li.menu-item-has-children').hover(function(e){
  if (window.innerWidth >= breakpoints.md) {
    e.stopPropagation();

    var toggleMenu = function($this, type, event) {
      menuToggle($this, type, event);
    };
    
    showMenuTimer = setTimeout(toggleMenu, 200, $(this), 'hover-in', e);
  }
}, function(e) {
  if (window.innerWidth >= breakpoints.md) {
    clearTimeout(showMenuTimer);
    menuToggle($(this), 'hover-out', e);
  }
});