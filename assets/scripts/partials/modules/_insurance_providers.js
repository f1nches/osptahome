module($('.insurance-providers'), function(){
  var providersLabel = $('.insurance-providers label.providers-label');
  var providersIndicator = $('.insurance-providers .accordion-indicator');
  var providersList = $('.insurance-providers ul.providers-list');
  var mobileBp = false;

  if (window.innerWidth < breakpoints.sm) {
    providersList.hide();
    mobileBp = true;
  }

  providersLabel.click(function() {
    var targetLabel = $(this);
    var indicator = targetLabel.find('.accordion-indicator');
    
    if (!targetLabel.hasClass('opened')) {
      indicator.text('-');
      targetLabel.addClass('opened');
    } else {
      indicator.text('+');
      targetLabel.removeClass('opened');
    }

    providersList.slideToggle();
  });

  $(window).resize(function(){
    if (mobileBp && window.innerWidth >= breakpoints.sm) {
      mobileBp = false;
      providersList.show();

      if (providersLabel.hasClass('opened')) {
        providersLabel.removeClass('opened');
        providersIndicator.text('+');
      }
    } else if (!mobileBp && window.innerWidth < breakpoints.sm) {
      mobileBp = true;
      providersList.hide();
    }
  });
});