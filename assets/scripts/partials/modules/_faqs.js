module($('.faqs-module'), function(){
  var faqQuestion = $('.faqs-module .question');

  faqQuestion.click(function(){
    var question = $(this);
    question.toggleClass('opened');
    question.next().slideToggle();
  });
});