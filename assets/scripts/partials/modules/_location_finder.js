ospta.locationFinder = {
_locations: {},
_map: null,
_infoWindow: null,
_baseURL: '',
_bounds: null,

init: function (lat, lng, base_url) {
    this._map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: lat, lng: lng},
        scrollwheel: false,
        zoom: 11
    });
    
    this._bounds = new google.maps.LatLngBounds();
    
    this._infoWindow = new google.maps.InfoWindow();
    
    this._baseURL = base_url;

    this._getLocationsAJAX();
    this._bindEvents();
},
_bindEvents: function () {
    var self = this;
    $('#search').on('click', function (e) {
        e.preventDefault();
        var zip = $('#zip').val();
        var radius = $('#radius').val();
        var service = $('#service').val();
        var physician = $('#physician').val();

        self.doSearch(zip, radius, service, physician);
    });
    
    $('body').on('click', '.litem', function(e) {
        var id = $(this).data('id');
        
        google.maps.event.trigger(self._locations[id].marker, 'click' );
    });
},
_addLocation: function (location) {
    var self = this;
    var marker = new google.maps.Marker({
        position: {lat: parseFloat(location.latitude), lng: parseFloat(location.longitude)},
        map: this._map,
        icon: this._baseURL + '/assets/images/marker.png'
    });


    marker.addListener('click', function () {
        self._infoWindow.setContent(location.content);
        self._infoWindow.open(this._map, marker);
        $('.litem').removeClass('active');
        $('#location-'+location.id).addClass('active');
    });

    location.directions_url = location.location_address+"+"+location.location_city+",+"+location.location_state+"+" + location.location_zip_code;
    location.content = "<h3>"+ location.name +"</h3><br><i class='fa fa-address-card' aria-hidden='true'></i> "+location.location_address+"<br>"+location.location_city+", "+location.location_state+" " + location.location_zip_code+"<br><i class='fa fa-phone' aria-hidden='true'></i> "+ location.location_phone_number +"<br><br><a style='color: blue;' href='"+ location.url +"'>View More Information</a> | <a style='color: blue;' target='_blank' href='https://www.google.com/maps/search/?api=1&query="+ location.directions_url +"'>Get Directions</a>";
    location.marker = marker;
    location.hidden = false;
    
    $('#the-list').append("<li class='litem' data-id='"+ location.id +"' id='location-"+ location.id +"'><h3>"+ location.name +"</h3><br><i class='fa fa-address-card' aria-hidden='true'></i> "+location.location_address+"<br>"+location.location_city+", "+location.location_state+" " + location.location_zip_code+"<br><i class='fa fa-phone' aria-hidden='true'></i> "+ location.location_phone_number+"</li>");

    this._locations[location.id] = location;
},
_hideLocations: function () {
    for (var i in this._locations) {
        var location = this._locations[i];
        if (location.hidden) {
            location.marker.setMap(null);
            $('#location-'+location.id).hide();
        } else {
            location.marker.setMap(this._map);
            $('#location-'+location.id).show();
        }
    }

    return true;
},
_filterByService: function (service) {
    if (!service)
        return false;

    for (var i in this._locations) {
        var location = this._locations[i];
        if (location.hidden)
            continue; // already been filtered, so move on

        location.hidden = !location.services.hasOwnProperty(service);
    }

    return true;
},
_filterByID: function (ids) {
    if (!ids)
        return false;

    for (var i in this._locations) {
        var location = this._locations[i];
        if (location.hidden)
            continue; // already been filtered, so move on

        location.hidden = ids.indexOf(location.id) === -1;
    }

    return true;
},
_showLoader: function () {
    $('#loading-screen').fadeIn(100);
},
_hideLoader: function () {
    $('#loading-screen').fadeOut(100);
},

_setBounds: function() {
    for(var i in this._locations) {
        var location = this._locations[i];
        this._bounds.extend({lat: parseFloat(location.latitude), lng: parseFloat(location.longitude)});
    }
},

_filterByTherapist: function (therapist) {
    if (!therapist)
        return false;

    for (var i in this._locations) {
        var location = this._locations[i];
        if (location.hidden)
            continue; // already been filtered, so move on

        location.hidden = !location.therapists.hasOwnProperty(therapist);
    }

    return true;
},
_resetHiddenStatusForLocations: function () {
    for (var i in this._locations) {
        var location = this._locations[i];

        location.hidden = false;
    }

    return true;
},
doSearch: function (zip, radius, service, therapist) {
    this._resetHiddenStatusForLocations();
    var self = this;
    this._bounds = new google.maps.LatLngBounds();

    if (zip && radius) {
        // ajax call
        this._showLoader();

        $.ajax({
            url: ajax_url,
            data: {action: 'get_locations_in_radius', zip: zip, radius: radius},
            type: 'POST',
            dataType: 'JSON'
        })
                .success(function (ids) {
                    self._afterSearchDoCenter(zip, service, therapist, ids);
                })
                .always(function () {
                    self._hideLoader();
                });

    } else {
        this._filterByService(service);
        this._filterByTherapist(therapist);
        this._hideLocations();
    }


},
_afterSearchDoCenter: function (zip, service, therapist, ids) {
    var self = this;
    
    $.ajax({
        url: ajax_url,
        data: {action: 'zip_to_latlng', zip: zip},
        type: 'POST',
        dataType: 'JSON'
    })
            .success(function (coords) {
                self._map.panTo(coords);
                self._filterByID(ids);
                self._filterByService(service);
                self._filterByTherapist(therapist);
                self._hideLocations();
                self._setBounds();
                self._map.fitBounds(self._bounds);
            })
            .always(function () {
                self._hideLoader();
            });
},
_getLocationsAJAX: function () {
    var self = this;
    $.ajax({
        url: ajax_url,
        data: {action: 'get_locations'},
        type: 'POST',
        dataType: 'JSON'
    })
            .success(function (locations) {
                for (var i in locations) {
                    self._addLocation(locations[i]);
                }
            });
}
};

