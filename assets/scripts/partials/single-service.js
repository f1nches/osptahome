module($('aside.all-services'), function(){

  var accordionLabel = $('aside.all-services label');
  var serviceMobile;

  if (window.innerWidth < breakpoints.sm) {
    serviceMobile = false;
  } else {
    serviceMobile = true;
  }

  accordionLabel.click(function(){
    $(this).toggleClass('opened');
    $(this).siblings('.services-wrapper').slideToggle();
  });

  $(window).resize(function(){

    if (serviceMobile && window.innerWidth >= breakpoints.sm) {
      serviceMobile = false;
      $('aside.all-services .services-wrapper').attr('style', '');
    } else if (!serviceMobile && window.innerWidth < breakpoints.sm) {
      serviceMobile = true;
    }
  });
});