function Slider(customOptions, customBpOptions){
  this.defaultOptions = {
    sliderClass: 'slider',
    slideClass: 'slide',
    startBp: '',
    infiniteLoop: true, // bp
    numberOfSlides: 1, // bp
    showIndicators: true, // bp
    indicatorAlignment: 'center', // bp
    showArrows: true, // bp
    animationType: false, // bp
    indicatorSource: 'fa',
    faIndicatorSelected: 'circle',
    faIndicatorSelectedColor: 'black',
    faIndicatorUnselected: 'circle-o',
    faIndicatorUnselectedColor: 'black',
    imgIndicatorSelectedUrl: '',
    imgIndicatorUnselectedUrl: '',
    arrowSource: 'fa',
    faArrowRight: 'chevron-right',
    faArrowLeft: 'chevron-left',
    imgArrowRightUrl: '',
    imgArrowLeftUrl: '',
  };

  this.options = $.extend(this.defaultOptions, customOptions);
  this.bpOptions = customBpOptions;
  this.slider = $('.'+this.options.sliderClass);
  this.sliderOn = false;
  this.showingIndicators = false;
}

Slider.prototype.initStartingSlides = function() {
  var slidesToShow = this.options.numberOfSlides;
  var slideClass = this.options.slideClass;
  this.slider.each(function(){
    var sliderSlides = $(this).find('.'+slideClass);
    for (var i = 0; i < sliderSlides.length; i++) {
      var startingSlide = $(sliderSlides[i]);
      startingSlide.attr('data-slidenum', i+1);
      if (i < slidesToShow) {
        
        startingSlide.addClass('current-slide');
      }
    }
  });
};

Slider.prototype.hideNonStartingSlides = function() {
  var nonStartingSlides = this.slider.find('.'+this.options.slideClass).not('.current-slide');
  nonStartingSlides.addClass('hide');
};

Slider.prototype.renderIndicators = function() {
  this.showingIndicators = true;
  var slideClass = this.options.slideClass;
  var indicatorSource = this.options.indicatorSource;
  var faIndicatorSelected = this.options.faIndicatorSelected;
  var faIndicatorSelectedColor = this.options.faIndicatorSelectedColor;
  var faIndicatorUnselected = this.options.faIndicatorUnselected;
  var faIndicatorUnselectedColor = this.options.faIndicatorUnselectedColor;
  var indicatorAlignment = this.options.indicatorAlignment;

  this.slider.each(function(){
    var startingSlides = $(this).find('.'+slideClass);
    var indicators = '<div class="slider-indicators '+indicatorAlignment+'-text">';
    var isCurrent;
    for (var i = 0; i < startingSlides.length; i++) {
      isCurrent = $(startingSlides[i]).hasClass('current-slide');
      if (indicatorSource === 'fa' && isCurrent) {
        indicators += '<i class="slider-indicator active-indicator fa fa-'+faIndicatorSelected+' color-'+faIndicatorSelectedColor+'" data-slidenum="'+(i+1)+'"></i>';
      } else if (indicatorSource === 'fa' && !isCurrent) {
        indicators += '<i class="slider-indicator fa fa-'+faIndicatorUnselected+' color-'+faIndicatorUnselectedColor+'" data-slidenum="'+(i+1)+'"></i>';
      } else if (indicatorSource === 'img' && isCurrent) {
        indicators += '<img class="slider-indicator active-indicator" src="'+imgIndicatorSelectedUrl+' alt="Selected Indicator" data-slidenum="'+(i+1)+'">';
      } else if (indicatorSource === 'img' && !isCurrent) {
        indicators += '<img class="slider-indicator" src="'+imgIndicatorUnselectedUrl+' alt="Unselected Indicator" data-slidenum="'+(i+1)+'">';
      }
    }
    indicators += '</div>';
    $(this).append(indicators);
  });
};

Slider.prototype.handleIndicators = function() {
  var options = this.options;
  var slider = this.slider;
  var sliderIndicators = slider.find('.slider-indicator');
  sliderIndicators.on('click', function(){
    var clickedIndicator = $(this);

    if (!clickedIndicator.hasClass('active-indicator')) {
      var currentIndicator = clickedIndicator.siblings('.active-indicator');
      var slideNum = clickedIndicator.data('slidenum');
      var currentSlide = clickedIndicator.parents('.'+options.sliderClass).find('.current-slide');
      var associatedSlide = clickedIndicator.parents('.'+options.sliderClass).find('.'+options.slideClass+'[data-slidenum='+slideNum+']');
      currentSlide.addClass('hide').removeClass('currentSlide');
      associatedSlide.removeClass('hide').addClass('current-slide');
      currentIndicator.removeClass('active-indicator');
      clickedIndicator.addClass('active-indicator');

      if (options.indicatorSource === 'fa') {
        var indicatorClassToggling = 'fa-'+options.faIndicatorSelected+' color-'+options.faIndicatorSelectedColor+' fa-'+options.faIndicatorUnselected+' color-'+options.faIndicatorUnselectedColor;
        currentIndicator.toggleClass(indicatorClassToggling);
        clickedIndicator.toggleClass(indicatorClassToggling);
      }
    }
  });
};

Slider.prototype.start = function(bp) {
  console.log(bp);
  var bpOptions = this.bpOptions[bp];
  if (!bp) {
    this.sliderOn = true;
    this.initStartingSlides();
    this.hideNonStartingSlides();

    if (this.options.showIndicators) {
      this.renderIndicators();
      this.handleIndicators();
    }

    // if (this.options.showArrows) {
    //   this.renderArrows();
    //   this.handleIndicators();
    // }
  } else {

    if (!this.sliderOn) {
      this.initStartingSlides();
      this.hideNonStartingSlides();
    }
  }
};

Slider.prototype.stop = function() {
  this.sliderOn = false;
  this.showingIndicators = false;
  this.slider.find('.current-slide').removeClass('current-slide');
  this.slider.find('.hide').removeClass('hide');
  this.slider.find('.slider-indicators').remove();
};

Slider.prototype.bpChange = function(bp) {
  var bpOptions = this.bpOptions[bp];
  if (!this.sliderOn && this.options.startBp === bp) {
    this.start(bp);
  }
  if (!this.sliderOn && bpOptions.restartSlider) {
    this.start(bp);
  }
  if (this.sliderOn && bpOptions.stopSlider) {
    this.stop();
  }

  if (!this.showingIndicators && (bpOptions.showIndicators || (this.options.showIndicators && !bpOptions.hasOwnProperty('showIndicators')))) {
    this.showingIndicators = true;
    this.renderIndicators();
    this.handleIndicators();
  }

  if (this.showingIndicators && (!bpOptions.showIndicators || (!this.options.showIndicators && !bpOptions.hasOwnProperty('showIndicators')))) {
    this.showingIndicators = false;
    this.slider.find('.slider-indicators').remove();
  }
};

Slider.prototype.init = function() {
  var obj = this;
  var windowWidth = window.innerWidth;
  if (this.options.startBp === '') {
    this.start();
  } else {
    if (windowWidth >= breakpoints[this.options.startBp]) {
      this.start();
    } else {
      this.sliderOn = false;
    }
  }

  if (!$.isEmptyObject(this.bpOptions)) {

    $(window).on('resize', function(){
      var windowWidth = window.innerWidth;
      if (!obj.sliderOn && obj.options.startBp === '') {
        obj.start();
      } else if (obj.options.startBp !== '' && windowWidth < breakpoints[obj.options.startBp] && obj.sliderOn) {
        obj.stop();
      } else if (obj.options.startBp !== '' && windowWidth >= breakpoints[obj.options.startBp] && !obj.sliderOn) {
        obj.start();
      }

      for (var bp in obj.bpOptions) {

        if (windowWidth >= breakpoints[bp]) {
          obj.bpChange(bp);
        }
      }
    });
  }
};

// Slider.prototype.change = function() {
  
// };