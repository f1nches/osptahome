/***************
 * BREAKPOINTS *
 ***************/
var rootTag = document.documentElement;
var fontSizeVal = window.getComputedStyle(rootTag, null).getPropertyValue('font-size');
var rem = parseFloat(fontSizeVal);

var breakpoints = {
  xs: 30*rem, // 480px assuming 16px rem
  sm: 40*rem, // 640px assuming 16px rem
  md: 48*rem, // 768px assuming 16px rem
  lg: 64*rem, // 1024px assuming 16px rem
  xl: 85.375*rem // 1366px assuming 16px rem
};

/******************
 * AJAX DIRECTORY *
 ******************/

var ajaxDir = '/wp-content/themes/basetheme/ajax/';

/***************
 * MEDIA COVER *
 ***************/
/*
** These functions are to have have media behave like background-size: cover
*/

function adjustDimensions(item, minW, minH, maxW, maxH) {
  item.css({
  minWidth: minW,
  minHeight: minH,
  maxWidth: maxW,
  maxHeight: maxH
  });
} // end function adjustDimensions

function mediaCoverBounds() {
  var mediaCover = $('.media-cover');

  mediaCover.each(function() {
    var image = $(this);
    var mediaWrapper = image.parents('.media-cover-wrapper');
    adjustDimensions(image, '', '', '', '');
    if (image.hasClass('hide')) {
      image.removeClass('hide');

      var background = mediaWrapper.css('background');

      if (typeof background !== typeof undefined && background !== false) {
        image.parent().css('background', '');
      }
    }
    
    var mediaWrapperWidth = mediaWrapper.outerWidth();
    var mediaWrapperHeight = mediaWrapper.outerHeight();
    var mediaCoverWidth = image.outerWidth();
    var mediaCoverHeight = image.outerHeight();
    var maxCoverWidth;
    var maxCoverHeight;

    if (mediaCoverWidth > mediaWrapperWidth && mediaCoverHeight > mediaWrapperHeight) {

      if (mediaWrapperHeight/mediaWrapperWidth > mediaCoverHeight/mediaCoverWidth) {
        maxCoverWidth = '';
        maxCoverHeight = '100.1%';
      } else {
        maxCoverWidth = '100.1%';
        maxCoverHeight = '';
      } // end if

      adjustDimensions(image, '', '', maxCoverWidth, maxCoverHeight);
    } else {
      adjustDimensions(image, '100.1%', '100.1%', '', '');
    } // end if
  }); // end mediaCover.each
} // end function mediaCoverBounds

$(window).on('load', function(){
  mediaCoverBounds();
});

$(window).on('resize', function(){
  mediaCoverBounds();
});

// Get Url Vars

function getUrlVars() {
  var vars = [], hash;
  var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
  for(var i = 0; i < hashes.length; i++)
  {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
  }
  return vars;
}

var getQuery = getUrlVars();

/*******************
 * MODULE FUNCTION *
 *******************/

/*
* Only runs module scripts if a selector exists
*/
function module(selector, callback) {
  if (selector.length !== 0) {
    callback();
  }
}