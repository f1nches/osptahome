var footer = $('footer.footer');
var footerHeight = footer.outerHeight();
var bodyWrapper = $('.body-wrapper');
var footerPush = $('.footer-push');

bodyWrapper.css('margin-bottom', '-'+footerHeight+'px');
footerPush.css('height', footerHeight+'px');

$(window).load(function(){
  footerHeight = footer.outerHeight();
  bodyWrapper.css('margin-bottom', '-'+footerHeight+'px');
  footerPush.css('height', footerHeight+'px');
});

$(window).resize(function(){
  footerHeight = footer.outerHeight();
  bodyWrapper.css('margin-bottom', '-'+footerHeight+'px');
  footerPush.css('height', footerHeight+'px');
});

// $('.accordion').find('.header').on('click', function (e) {
//     var $acc = $(this).parent();
//     var is_open = $acc.hasClass('open');

//     if (is_open) {
//         $acc.removeClass('open');
//         $acc.find('.header').removeClass('open');
//         $acc.find('.header').find('.fa-arrow-right').removeClass('open');
//         $acc.find('.body').removeClass('open');
//     } else {
//         $acc.addClass('open');
//         $acc.find('.header').addClass('open');
//         $acc.find('.header').find('.fa-arrow-right').addClass('open');
//         $acc.find('.body').addClass('open');
//     }
// });

module('.toggler', function(){

  $('.toggler').find('li').on('click', function (e) {
    e.preventDefault();

    $('.panel-map').hide();
    $('.panel-list').hide();
    $('.toggler').find('li').removeClass('active');
    $(this).addClass('active');
    var show = $(this).data('panel');
    $('.panel-' + show).show();
    google.maps.event.trigger(ospta.locationFinder._map, 'resize');
  });
});