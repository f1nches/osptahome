<?php
  global $fields;
  $heroImage = isset($fields['hero_image']) ? $fields['hero_image'] : '';
  $heroSlides = isset($fields['hero_slides']) ? $fields['hero_slides'] : '';
  $heroCopy = isset($fields['hero_copy']) ? $fields['hero_copy'] : '';
  $heroCtaLink = isset($fields['hero_cta_link']) ? $fields['hero_cta_link'] : '';
  $heroCtaText = isset($fields['hero_cta_text']) ? $fields['hero_cta_text'] : '';

  if (is_front_page()) {
    $navWrapperClass = 'home-nav';
  } else {
    $navWrapperClass = 'interior-nav';
  }
?>
<header id="page-header">
  <div class="topbar">
    <div class="container-sm">
      <div class="phone-number col-sm-4 col-md-6">
          <a class="phone" href="tel:18003376452">1-800-337-6452</a><span class="separator">|</span><a class="email" href="mailto:info@osptainc.com">INFO@OSPTAINC.COM</a>
      </div>
      <div class="col-sm-2 col-md-2" style="float: right;">
          <?php get_search_form(); ?>
      </div>
    </div>
  </div>
  <div class="container-md nav-wrapper <?php echo $navWrapperClass; ?>">
    <?php include('partials/_nav.php'); ?>
  </div>


  <?php if (is_front_page() && notEmpty($heroSlides)) : ?>

      <div class="slides-container">
        <?php foreach ($heroSlides as $heroSlide) : ?>
          <?php
            $heroImage = $heroSlide['hero_image'];
            $heroBoxType = $heroSlide['hero_box_type'];
            $heroCtaText = $heroSlide['hero_cta_text'];
            $heroCtaLink = $heroSlide['hero_cta_link'];
          ?>
          <div class="header-bottom">
          <div class="hero">
            <div class="hero-image-wrapper" style="background: url('<?php echo $heroImage['url']; ?>') no-repeat center center / cover; height: 600px;"></div>

            <?php if ($heroBoxType === 'text' && notEmpty($heroSlide['hero_copy'])) : ?>
              <div class="hero-copy">
                <?php echo $heroSlide['hero_copy']; ?>
              </div>
            <?php endif; ?>

            <?php if ($heroBoxType === 'logo') : ?>
              <div class="hero-copy hero-logo">
                <img src="<?= get_template_directory_uri() ?>/assets/images/ospta.svg" class="logo" alt="OSPTA">
              </div>
            <?php endif ?>

          </div>

          <?php if (notEmpty($heroCtaText) && notEmpty($heroCtaLink)) : ?>
            <div class="hero-cta center-text">
              <a class="button background-gold color-white large-button" href="<?php echo $heroCtaLink; ?>" class="hero-cta-link"><?php echo $heroCtaText; ?></a>
            </div>
          <?php endif; ?>
          </div>
        <?php endforeach; ?>
      </div>

  <?php endif; ?>
</header>
