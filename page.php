<?php
  $fields = get_fields();
  $options = get_fields('option');
?>
<?php include('partials/head.php'); ?>
<?php get_header(); ?>
<main class="interior <?=$post->post_name?>">
    <?php include('acf_page_modules.php'); ?>
</main>
<?php get_footer(); ?>