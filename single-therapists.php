<?php
include('partials/head.php');
$query = new WP_Query(array(
    'post_type' => 'locations',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'orderby' => 'title',
    'order' => 'ASC',
        ));

$locations = [];
$post_id = $post->ID;

while ($query->have_posts()) {
    $query->the_post();
    $loc_data = [
        'slug' => get_the_permalink(),
        'name' => get_the_title()
    ];
    if (have_rows('location_physical_therapists')) {

        while (have_rows('location_physical_therapists')) {
            the_row();
            $therapist = get_sub_field('physical_therapist');
            if($therapist->ID == $post_id) {
                $locations[] = $loc_data;
            }
        }
    }
}
wp_reset_query();
?>
<?php get_header(); ?>
<?php
$fields = get_fields();
$options = get_fields('option');
?>
<main class="interior page-location">
<?php force_load_module('current_page_area', ['name' => 'OSPTA - ' . get_the_title()]); ?>
    <ul class="breadcrumbs container-sm hide show-block-sm">
        <li><a href="<?= site_url() ?>">Home</a></li>
        <li><?= get_the_title() ?></li>
    </ul>
    <section class="general-info container-lg">
        <h3>About <?= get_the_title() ?> <?= get_field('title') ?></h3>
        <p><?= get_field('info'); ?></p>
        <h3>Locations</h3>
        <ul>
            <?php foreach($locations as $loc): ?>
            <li><a href="<?=$loc['slug']?>"><?=$loc['name']?></a></li>
            <?php endforeach; ?>
        </ul>    
    </section>
</main>

<?php get_footer(); ?>