<?php include('partials/head.php'); ?>
<?php get_header(); ?>
<?php
$fields = get_fields();
$options = get_fields('option');
?>
<main class="interior page-location">
    <?php force_load_module('current_page_area', ['name' => 'OSPTA - ' . get_the_title()]); ?>
    <ul class="breadcrumbs container-sm hide show-block-sm">
        <li><a href="<?= site_url() ?>">Home</a></li>
        <li><a href="<?= site_url() ?>/locations/">Locations</a></li>
        <li><?= get_the_title() ?></li>
    </ul>
    <section class="general-info container-lg">
        <div class="col-xl-7 container copy-container">
            <div class= "address-container copy-section col-sm-5 row-sm-2 col-md-4">
                <h2 class="copy-section-title small-blue-title">Address</h2>
                <div class="address">
                    <p><?= $fields['location_address']; ?></p>
                    <p><?= $fields['location_city']; ?>, <?= $fields['location_state']; ?> <?= $fields['location_zip_code']; ?></p>
                    <p style="margin-bottom: 20px; margin-top: 5px;"><a style="color: blue;" target="_blank" href="https://www.google.com/maps/search/?api=1&query=<?= $fields['location_address']; ?>+<?= $fields['location_city']; ?>+<?= $fields['location_state']; ?>+<?= $fields['location_zip_code']; ?>"><i class="fa fa-external-link-square" aria-hidden="true"></i> Get Directions</a></p>
                    <?php if (notEmpty($fields['location_phone_number'])) : ?>
                        <p>T: <?= $fields['location_phone_number']; ?></p>
                    <?php endif; ?>

                    <?php if (notEmpty($fields['location_fax'])) : ?>
                        <p>F: <?= $fields['location_fax']; ?></p>
                    <?php endif; ?>

                    <?php if (notEmpty($fields['location_email'])) : ?>
                        <p>E: <a href="mailto:<?= $fields['location_email']; ?>"><?= $fields['location_email']; ?></a></p>
                    <?php endif; ?>

                </div>
            </div>
            <div class="col-sm-7 row-sm-2 col-md-8 copy-section hours-container">
                <?php
                $locationOfficeHours = $fields['location_office_hours'];
                $otherHours = $fields['other_hours'];
                ?>
                <?php if (notEmpty($locationOfficeHours)) : ?>
                    <h2 class="copy-section-title small-blue-title">Office Hours</h3>
                    <?php endif; ?>
                    <ul class="office-hours no-style-list">

                        <?php if (notEmpty($locationOfficeHours)) : ?>

                            <?php foreach ($locationOfficeHours as $officeHours) : ?>
                                <?php
                                $locationDay = $officeHours['location_day'];
                                $locationHoursStart = $officeHours['location_hours_start'];
                                $locationHoursEnd = $officeHours['location_hours_end'];
                                ?>
                                <li><span class="day"><?= $locationDay; ?></span> <?= $locationHoursStart ?> <?php if (!empty($locationHoursEnd)): ?>to <?= $locationHoursEnd; ?><?php endif; ?></li>
                            <?php endforeach; ?>

                        <?php else : ?>
                            
                        <?php endif; ?>
                    </ul>
                    <?php if (notEmpty($otherHours)): ?>
                        <?php $otherHours = $otherHours[0];
                        ?>
                        <h2 class="copy-section-title small-blue-title" style="margin-top: 20px;"><?= $otherHours['hours_title'] ?></h2>
                        <ul class="office-hours no-style-list">
                            <?php foreach ($otherHours['hours'] as $oHours): ?>
                                <?php
                                $locationDay = $oHours['day'];
                                $locationHoursStart = $oHours['hours_start'];
                                $locationHoursEnd = $oHours['hours_end'];
                                ?>
                                <li><span class="day"><?= $locationDay; ?></span> <?= $locationHoursStart ?> <?php if (!empty($locationHoursEnd)): ?>to <?= $locationHoursEnd; ?><?php endif; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
            </div>

            <?php
            $locationServices = $fields['location_services'];
            ?>

            <?php if (notEmpty($locationServices)) : ?>
                <div class="col-sm-5 row-sm-2 col-md-4 copy-section services-container">
                    <h2 class="copy-section-title small-blue-title">Services Offered</h2>
                    <ul class="services-offered">

                        <?php foreach ($locationServices as $service) : ?>
                            <?php
                            $serviceText = $service['service'];
                            ?>
                            <li><?= $serviceText ?></li>

                        <?php endforeach; ?>

                    </ul>
                </div>

            <?php endif; ?>

            <div class="copy-section therapists-container col-sm-7 row-sm-2 col-md-8">
                <?php
                if (have_rows('location_physical_therapists')):
                    echo '<h2 class="copy-section-title small-blue-title">Therapists</h2>';
                    while (have_rows('location_physical_therapists')) : the_row();
                        $therapist = get_sub_field('physical_therapist');
                        ?>
                        <div class="therapist">
                            <h3 class="therapist-title small-light-blue-title"><?= $therapist->post_title ?> <?= get_field('title', $therapist->ID) ?></h3>
                            <div class="therapist-info">
                                <?= get_field('info', $therapist->ID) ?>
                            </div>
                        </div>
                        <?php
                    endwhile;
                endif;
                ?>
            </div>
        </div>
        <div class="col-xl-5 container-md map-container">
            <div id="map" style="height: 400px;"></div>
            <script>
                function initMap() {
                    // Create a map object and specify the DOM element for display.
                    var map = new google.maps.Map(document.getElementById('map'), {
                        center: {lat: <?= get_field('latitude') ?>, lng: <?= get_field('longitude') ?>},
                        scrollwheel: false,
                        zoom: 13
                    });

                    var marker = new google.maps.Marker({
                        position: {lat: <?= get_field('latitude') ?>, lng: <?= get_field('longitude') ?>},
                        map: map,
                        icon: '<?= get_template_directory_uri() ?>/assets/images/marker.png'
                    });

                    var infowindow = new google.maps.InfoWindow();

                    google.maps.event.addListener(marker, "click", function (e) {
                        infowindow.close();
                        map.setCenter(marker.getPosition());
                        infowindow.open(marker);
                    });

                    infowindow.setContent("OSPTA - <?= get_the_title() ?><br><?= $fields['location_address']; ?>, <?= $fields['location_city']; ?> <?= $fields['location_state']; ?><br><?= $fields['location_zip_code']; ?><p style='margin-top: 5px;'><a style='color: blue;' target='_blank' href='https://www.google.com/maps/search/?api=1&query=<?= $fields['location_address']; ?>+<?= $fields['location_city']; ?>+<?= $fields['location_state']; ?>+<?= $fields['location_zip_code']; ?>'><i class='fa fa-external-link-square' aria-hidden='true'></i> Get Directions</a></p>");
                    infowindow.open(map, marker);

                    //google.maps.event.trigger(marker, 'click');
                }

                jQuery(document).ready(function () {
                    initMap();
                });

            </script>
        </div>
    </section>
</main>
<script src="https://maps.googleapis.com/maps/api/js?key=<?= get_field('google_maps_api_key', 'options') ?>&callback=initMap"
async defer></script>
<?php get_footer(); ?>