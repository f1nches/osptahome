<?php
$slug = $post->post_name;
?>
<?php include('partials/head.php'); ?>
<?php get_header(); ?>
<main class="interior page-service">
    <?php force_load_module('current_page_area', ['name' => get_the_title()]); ?>
    
    <div class='container-md'>
        <ul class="breadcrumps">
            <li><a href="<?=site_url()?>">Home</a></li>
            <li><a href="<?=site_url()?>/services/">Services</a></li>
            <li><a href="<?=site_url()?>/services/<?=$slug?>"><?=get_the_title()?></a></li>
            
        </ul>
        <nav class="col-md-3 all-services">
            <ul class="no-mobile">
                <?php
                $query = new WP_Query(array(
                    'post_type' => 'services',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'orderby' => 'title',
                    'order' => 'DESC'
                ));

                $nav = [];


                while ($query->have_posts()) {
                    $query->the_post();
                    $is_active = $slug == $post->post_name;
                    $active_text = $is_active ? ' class="active"' : '';

                    $nav[$post->post_name] = get_the_title();

                    echo '<li' . $active_text . '><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></li>';
                }

                wp_reset_query();
                ?>
            </ul>            
            <div class="choose-service accordion">
                <div class="header">Services
                    <div class="right-arrow"><i class="fa fa-arrow-right"></i></div>
                </div>
                <div class="body">
                    <ul>
                        <?php
                        $query2 = new WP_Query(array(
                            'post_type' => 'services',
                            'post_status' => 'publish',
                            'posts_per_page' => -1,
                            'orderby' => 'title',
                            'order' => 'DESC'
                        ));


                        foreach ($nav as $url => $n) {
                            $is_active = $slug == $url;
                            $active_text = $is_active ? ' selected' : '';
                            if (empty(trim($url)))
                                continue;

                            echo '<li>' . $n . '</li>';
                        }

                        wp_reset_query();
                        ?>
                    </ul>
                </div>
            </div>
        </nav>
        <article class="col-md-9">
            <div class="container">
                <?= get_template_part('partials/interior_hero') ?>
                <?= get_field('body_copy'); ?>
            </div>
        </article>
    </div>
</main>
<?php get_footer(); ?>