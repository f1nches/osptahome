<?php
global $wp_query;

include('partials/head.php');
?>
<?php get_header(); ?>
<main class="interior search location-finder-module">
    <?php force_load_module('current_page_area', ['name' => 'OSPTA - Search']); ?>
    <div class="container locations-container">
        <div class="panel-county">
            <h2 class="search-title" style="margin-top: 30px;"> <?php echo $wp_query->found_posts; ?>
                <?php _e('Search Results Found For', 'locale'); ?>: "<?php the_search_query(); ?>" </h2>

            <?php if (have_posts()) { ?>

                <div class="locations-container">

                    <?php while (have_posts()) {
                        the_post();
                        if($post->ID == '34' || $post->post_type == 'post') continue;
                        ?>

                        <div class="location-entry-wrapper">
                            <a href="<?php echo get_permalink(); ?>">
                                <div class="location-entry">
                                    <span class="location-title"><?php the_title(); ?></span>
                                    <br>
                                    <?=ucfirst(get_post_type())?>
                                </div>
                        </div>

    <?php } ?>

                </div>

                <?php paginate_links(); ?>

<?php } ?>

        </div>
    </div>
</main>
<?php get_footer(); ?>