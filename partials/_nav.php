<div class="logo-bars-container col-md-3">
  <div class="logo-wrapper col-9 col-md-12">
      <a href="<?= site_url() ?>"><img src="<?= get_template_directory_uri() ?>/assets/images/ospta.svg" class="logo" alt="OSPTA"></a>
  </div>
  <div class="col-3 hide-md menu-bars-wrapper">
    <i class="fa fa-bars"></i>
  </div>
</div>
<nav id="page-nav" class="col-12 col-md-9">
  <div class="menu-close-wrapper hide-md">
    <i class="color-white fa fa-times"></i>
  </div>
  <div class="mobile-logo-wrapper hide-md">
    <a href="<?php echo site_url(); ?>"><img class="company-logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/logo_footer.svg" alt="white OSPTA logo"></a>
  </div>
  <?php 
    $args = array(
      'theme_location' => 'main_nav',
      'menu_class' => 'nav-menu no-style-list',
      'container' => '',
      'container_class' => '',
      'container_id' => '',
    );
    wp_nav_menu($args);
  ?>
</nav>