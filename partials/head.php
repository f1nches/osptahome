<!DOCTYPE html>
<html lang="en-US">
    <head>
        <script src="https://use.fontawesome.com/3f09bd59bb.js"></script>
        <meta charset="utf-8">
        <?php if (!is_front_page()) : ?>
            <title><?php the_title(); ?> | <?php echo get_bloginfo('name'); ?></title>
        <?php else : ?>
            <title><?php echo get_bloginfo('name'); ?></title>
        <?php endif; ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php wp_head(); ?>
        <script src="https://use.typekit.net/zkj8hfz.js"></script>
        <script>try{Typekit.load({ async: true });}catch(e){}</script>
        <script>
            var ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";
            var ospta = {};
        </script>
    </head>
    <body>
    <div class="body-wrapper">