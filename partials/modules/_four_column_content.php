<?php
  $one = $module['column_one'];
  $two = $module['column_two'];
  $three = $module['column_three'];
  $four = $module['column_four'];
?>

  <div class="container four-column module">

    <div class="col-md-3 col-xs-6">
      <?php echo $one; ?>
    </div>

    <div class="col-md-3 col-xs-6">
      <?php echo $two; ?>
    </div>

    <div class="col-md-3 col-xs-6">
      <?php echo $three; ?>
    </div>

    <div class="col-md-3 col-xs-6">
      <?php echo $four; ?>
    </div>

  </div>
