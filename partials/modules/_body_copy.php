<div class="container body-copy module">

  <?php if(notEmpty($module['header'])) : ?>    
    <h2 class="small-blue-title body-title"><?= $module['header']?></h2>
  <?php endif; ?>

  <div class="main-copy cms-wysiwyg">
    <?= $module['content']; ?>
  </div>
</div>