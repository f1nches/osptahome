<div class="location-finder-module module">
    <div class="toggler-wrapper right-text container hide show-block-md">
        <ul class="toggler no-style-list center-text">
            <li data-panel="list" class="active">List View</li><li data-panel="county" class="hide-sm">County View</li><li data-panel="map" class="hide-sm">Map</li>
        </ul>
    </div>
    <div class="panel-map" style="display: none;">
        <div class="container location-finder">
            <form>
                <div class="col-sm-6 row-sm-2 col-lg-2 row-lg-4 choice input-choice">
                    <h3>Search</h3>
                    <div class="fake-input">
                        <label>Zip Code</label>
                        <input type="number" name="zip" id="zip">
                    </div>
                </div>
                <div class="col-sm-6 row-sm-2 col-lg-2 row-lg-4 choice select-choice">
                    <div class="fake-input">
                        <label>Within</label>
                        <select name="radius" id="radius">
                            <option value="5">5 Miles</option>
                            <option value="10">10 Miles</option>
                            <option value="25" selected>25 Miles</option>
                            <option value="50">50 Miles</option>
                            <option value="100">100 Miles</option>
                        </select>    
                    </div>
                </div>
                <div class="col-sm-6 row-sm-2 col-lg-3 col-lg-offset-1 row-lg-4 choice select-choice bar-apply">
                    <h3>Filter By</h3>
                    <div class="fake-input">
                        <label style="color: #fff">Service</label>
                        <select name="service" id="service">
                            <option value="">Services</option>
                            <?php foreach (ospta_get_services() as $slug => $service): ?>
                                <option value="<?= $slug ?>"><?= $service ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 row-sm-2 col-lg-4 row-lg-4 choice select-choice">
                    <div class="fake-input">
                        <label>Physical Therapist</label>
                        <select name="physician" id="physician">
                            <option value="">Physical Therapist</option>
                            <?php
                            $query = new WP_Query(array(
                                'post_type' => 'therapists',
                                'post_status' => 'publish',
                                'posts_per_page' => -1,
                                'orderby' => 'name',
                                'order' => 'ASC'
                            ));

                            while ($query->have_posts()) {
                                $query->the_post();
                                echo '<option value="' . $post->post_name . '">' . get_the_title() . '</option>';
                            }

                            wp_reset_query();
                            ?>
                        </select>
                    </div>
                </div>
                <div class="button-wrapper center-text">
                    <button id="search" class="button background-gold color-white small-button search-button">
                        Find A Location
                    </button>
                </div>
            </form>    
        </div>
        <div class="container-md map-wrapper">
            <div id="loading-screen" style="display: none;">
                <div class="loading-overlay">
                    <div class="loading-text">
                        <span class="fa fa-spin fa-spinner show-block"></span>
                        <span class="loading show-block">Loading...</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 listing">
                <ul id="the-list" class="no-style-list">
                    
                </ul>
            </div>  
            <div class="col-md-9">
                <div id="map">

                </div>
            </div>

        </div>
        <script>
            jQuery(document).ready(function () {
                ospta.locationFinder.init(40.439722, -79.976389, '<?= get_template_directory_uri() ?>');
            });
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?= get_field('google_maps_api_key', 'options') ?>"></script>
    </div>
    <div class="panel-list">
        <div class="container">
            <h2>Locations</h2>
            <div class="locations-container">
                <?php
                $query = new WP_Query(array(
                    'post_type' => 'locations',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'orderby' => 'title',
                    'order' => 'ASC',
                ));

                $nav = [];

                while ($query->have_posts()) {
                    $query->the_post();
                    $is_active = $slug == $post->post_name;
                    $active_text = $is_active ? ' class="active"' : '';

                    $nav[$post->post_name] = get_the_title();
  
                    
                    echo '<div class="col-sm-6 row-sm-2 col-md-4 row-md-3 location-entry-wrapper"><a href="' . get_the_permalink() . '"><div class="location-entry"><span class="location-title">' . get_the_title() . '</span><br>' . get_field('location_address') . '<br>' . get_field('location_city') . ', ' . get_field('location_state') . ' ' . get_field('location_zip_code') . '<br>' . get_field('location_phone_number') . '</div></a></div>';
                }
                wp_reset_query();
                ?>
            </div>
        </div>
    </div>
    <div class="panel-county">
        <div class="container">
            <h2>Counties</h2>
            <div class="locations-container">
                <?php
                $query = new WP_Query(array(
                    'post_type' => 'locations',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'orderby' => 'title',
                    'order' => 'ASC',
                ));

                $nav = [];
                $counties = [];

                while ($query->have_posts()) {
                    $query->the_post();
                    $is_active = $slug == $post->post_name;
                    $active_text = $is_active ? ' class="active"' : '';

                    $nav[$post->post_name] = get_the_title();
                    
                    $county = get_field('county');
                    if(empty($counties[$county])) {
                        $counties[$county] = [];
                    }
                    
                    $counties[$county][] = '<div class="col-sm-6 row-sm-2 col-md-4 row-md-3 location-entry-wrapper"><a href="' . get_the_permalink() . '"><div class="location-entry"><span class="location-title">' . get_the_title() . '</span><br>' . get_field('location_address') . '<br>' . get_field('location_city') . ', ' . get_field('location_state') . ' ' . get_field('location_zip_code') . '<br>' . get_field('location_phone_number') . '</div></a></div>';
                }
                
                ksort($counties);
                
                $last = '';
                foreach($counties as $c => $locations) {
                    echo '<h3 style="margin-top: 15px; margin-bottom: 15px;">'.$c.'</h3><div class="row">';
                    
                    foreach($locations as $location) {
                        echo $location;
                    }
                    
                    echo '</div>';
                }

                wp_reset_query();
                ?>
            </div>
        </div>
    </div>
</div>
