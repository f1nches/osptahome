<?php
  $testimonials = $module['testimonials'];
?>

<div class="testimonials container center-text module">

<?php foreach($testimonials as $testimonial) : ?>

  <div class="single-testimonial module container">
    <?php echo $testimonial['testimonial']; ?>
  </div>

<?php endforeach; ?>

</div>
