<div class="current-page background-light-gold">
    <div class="container">
        <h1 class="color-white"><?=$module['name']?></h1>
    </div>
</div>
<?php $heroImage = $fields['hero_image'];
?>
<?php if (notEmpty($heroImage)) : ?>
  <div class="interior-hero container-md">
    <?php theImage($heroImage, 'hero-image', true, 'hero-image-wrapper'); ?>
  </div>
<?php endif; ?>