<?php
  $banner = $module['banner_field'];
?>

<div class="banner-content-container container module">

  <div class="banner-content module container" style="margin-top: 0; margin-bottom: 0;">
    <?php echo $banner; ?>
  </div>

</div>
