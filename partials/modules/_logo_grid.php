<?php
  $lgLogos = $module['lg_logos'];
?>
<div class="logo-grid container center-text module">

  <?php foreach($lgLogos as $lgLogo) : ?>
    <?php
      $lgLogoImage = $lgLogo['lg_logo_image'];
      $lgLogoLinkType = $lgLogo['lg_logo_link_type'];

      if ($lgLogoLinkType === 'external') {
        $lgLogoLink = $lgLogo['lg_external_link'];
      } else if ($lgLogoLinkType === 'internal') {
        $lgLogoLink = $lgLogo['lg_internal_link']->guid;
      }

      if ($lgLogoLinkType !== 'none' && $lgLogo['lg_open_new_tab']) {
        $lgLinkTarget = ' target="_blank"';
      } else {
        $lgLinkTarget = '';
      }
    ?>
    <div class="logo-wrapper col-6 row-2 col-md-4 row-md-3">

      <?php if ($lgLogoLinkType !== 'none') : ?>
        <a class="logo-link" href="<?php echo $lgLogoLink; ?>" <?php echo $lgLinkTarget; ?>>
      <?php endif; ?>

        <?php theImage($lgLogoImage, 'lg-logo'); ?>

      <?php if ($lgLogoLinkType !== 'none') : ?>
        </a>
      <?php endif; ?>

    </div>
  <?php endforeach; ?>

</div>