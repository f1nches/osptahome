<?php
  $picture = $module['picture'];
  $content = $module['content'];
?>

<div class="picture-content-container container module">

  <div class="picture-content module container">
    <div class="col-md-6">
      <img src="<?php echo $picture ?>"/>
    </div>
    <div class="col-md-6">
      <div><?php echo $content ?></div>
    </div>
  </div>

</div>
