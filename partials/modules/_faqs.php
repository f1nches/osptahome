
<div class="faqs-module module container">  
  <dl class="faqs-list">  
    <?php foreach ($module['faqs'] as $faq): ?>
      <div class="faq-wrapper">
        <dt class="question small-blue-title"><?= $faq['question'] ?></dt>
        <dd class="answer hide"><?= $faq['answer']; ?></dd>
      </div>
    <?php endforeach; ?>
  </dl>
</div>