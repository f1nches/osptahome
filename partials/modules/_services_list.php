<div class="services-list-module module container">
  <div class="hide desktop-list show-block-lg">
  <?php ospta_services_list('col-lg-4', 3); ?>
  </div>
  <div class="mobile-list hide-lg">
  <?php ospta_services_list('col-sm-6'); ?>
  </div>
</div>