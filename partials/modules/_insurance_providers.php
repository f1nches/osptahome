<?php
  $providers = $module['providers'];
?>

<div class="insurance-providers module container">

  <label class="providers-label show-block hide-sm color-gold"><span class="label-text">View all providers</span><span class="accordion-indicator">+</span></label>

  <div class="providers-list-wrapper center-text">
    <ul class="providers-list no-style-list hide show-block-sm">

      <?php foreach($module['providers'] as $provider) : ?>
        <li class="provider col-sm-6 row-sm-2 col-md-4 row-md-3">
          <?=$provider['name']; ?>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
</div>