<?php
  // varInfo($module);
  $gcModuleTitle = $module['gc_header_module_title'];
  $gcModuleSub = $module['gc_header_module_subtitle'];
  $gcIntroCopy = $module['gc_header_intro_copy'];
  $gcHeaderCtas = $module['gc_header_module_header_ctas'];
  // varInfo($gcHeaderCtas);
?>

<section class="module general-content container">
  <header class="gc-header">
    <h2 class="module-title"><?php echo $gcModuleTitle; ?></h2>
    <p class="module-subtitle"><?php echo $gcModuleSub; ?></p>
  </header>
  <div class="intro-copy">
    <?php echo $gcIntroCopy; ?>
  </div>

  <?php if (!empty($gcHeaderCtas)) : ?>
    <div class="cta-container">

      <?php foreach ($gcHeaderCtas as $gcCta) : ?>
        <?php
          // varInfo($gcCta);
          $ctaText = $gcCta['cta_text'];
          $ctaLinkLocation = $gcCta['cta_link_location'];
          $openNewTab = $gcCta['open_new_tab'];

          if ($ctaLinkLocation === 'external') {
            $ctaLink = $gcCta['cta_external_link'];
          } else {
            $ctaLink = $gcCta['cta_internal_link']->guid;
          }

          if ($openNewTab) {
            $ctaTarget = ' target="_blank"';
          } else {
            $ctaTarget = '';
          }
        ?>
        <a href="<?php echo $ctaLink; ?>"<?php echo $ctaTarget; ?>>
          <?php echo $ctaText; ?>
        </a>
      <?php endforeach; ?>

    </div>
  <?php endif; ?>

</section>