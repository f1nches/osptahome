<?php
  $gallery = $module['picture'];
?>

<style>
img {
  width: 100%;
}
.gallery-image {
  height: 300px;
  width: 100%;
  transition: 0.2s;
}
.single-picture {
  vertical-align:top;
  margin-bottom: 0;
  text-align: center;
}
.caption-container {
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  background: rgba(0,92,171,0.7);
  opacity: 0;
  transition: 0.2s;
}
.picture-caption {
  padding: 10px;
  box-sizing: border-box;
  color: #fff;
}
.gallery-image:hover .caption-container {
  opacity: 1;
  transition: 0.2s;
}
</style>

<div class="gallery container module row">

<?php foreach($gallery as $picture) : ?>

  <div class="single-picture module container col-md-4">
    <div class="gallery-image" style="background: url('<?php echo $picture['image']; ?>') no-repeat center center/cover">
      <div class="caption-container">
        <div class="picture-caption">
          <?php echo $picture['caption']; ?>
        </div>
      </div>
    </div>

  </div>

<?php endforeach; ?>

</div>
