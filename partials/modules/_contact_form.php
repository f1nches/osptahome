<section class="contact-form container-md module">
    <div class="form-container col-md-5">
        <h2 class="form-title small-blue-title">Send Us a Message</h3>
        <?php echo do_shortcode($module['shortcode']); ?>
    </div>
    <div class="map-container col-md-7">
        <div id="map"></div>
        <script>
            function initMap() {
                // Create a map object and specify the DOM element for display.
                var map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: 40.439722, lng: -79.976389},
                    scrollwheel: false,
                    zoom: 8
                });
            }

            jQuery(document).ready(function () {
                initMap();
            });

        </script>
    </div>
</section>
<script src="https://maps.googleapis.com/maps/api/js?key=<?=get_field('google_maps_api_key', 'options')?>&callback=initMap"
async defer></script>