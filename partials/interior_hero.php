<?php 
$hero = get_field('hero_image'); 
?>


<?php if(notEmpty($hero)) : ?>
  <div class="hero">
    <?php theImage($hero, 'hero-image', true, 'hero-image-wrapper'); ?>
  </div>
<?php endif; ?>