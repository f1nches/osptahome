<?php include('partials/head.php'); ?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="interior page-location">
    <?php force_load_module('current_page_area', ['name' => 'Employee Portal']); ?>
    <div class="container file-list" style="margin-top: 4.5rem;">
    <?php the_content(); ?>
    </div>
</main>
<?php get_footer(); ?>