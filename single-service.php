<?php
$slug = $post->post_name;
$fields = get_fields();
$options = get_fields('option');
$hero = $fields['hero_image'];

if (!notEmpty($hero)) {
  $whetherHero = ' no-hero';
} else {
  $whetherHero = '';
}
?>
<?php include('partials/head.php'); ?>
<?php get_header(); ?>
<main class="interior page-service">
  <?php force_load_module('current_page_area', ['name' => get_the_title()]); ?>

  <ul class="breadcrumbs container-sm hide show-block-sm">
    <li><a href="<?=site_url()?>">Home</a></li>
    <li><a href="<?=site_url()?>/services/">Services</a></li>
    <li><a href="<?=site_url()?>/services/<?=$slug?>"><?=get_the_title()?></a></li>
  </ul>
    
  <div class='container-sm'>
    <aside class="col-sm-6 col-md-5 col-lg-4 col-xl-3 all-services">
      <label class="hide-sm background-blue color-white">SERVICES</label>
      <div class="services-wrapper hide show-block-sm">
        <?php ospta_services_list(false, 1, $slug); ?>
      </div>
    </aside>
    <article class="service col-sm-6 col-md-7 col-lg-8 col-xl-9">
      <?= get_template_part('partials/interior_hero') ?>
      <div class="service-body cms-wysiwyg<?php echo $whetherHero; ?>">
        <?= $fields['body_copy']; ?>
      </div>
    </article>
  </div>
</main>
<?php get_footer(); ?>