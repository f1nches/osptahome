<?php
  $fields = get_fields();
  $options = get_fields('option');
  include('partials/head.php');
  get_header();
  include('acf_page_modules.php');
?>
<main class="interior container-md page-<?= $post->post_name ?>">
    <div class="main-callout module container-sm">
        <div class="col-sm-12 col-lg-12 intro-text-container">
          <section class="intro-text cms-wysiwyg">
            <?= $fields['intro_text']; ?>
          </section>
          <article class="service col-sm-6 col-md-7 col-lg-8 col-xl-9">
            <div class="service-body cms-wysiwyg<?php echo $whetherHero; ?>">
              <?= $fields['body_copy']; ?>
            </div>
          </article>
        </div>
    </div>
</main>
<?php get_footer(); ?>
